import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { rootReducer } from "./reducer";
import { Navigator } from "./routes";
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default function App() {
  const { ContactNavigator } = Navigator;
  return (
    <Provider store={store}>
      <ContactNavigator />
    </Provider>
  );
}
