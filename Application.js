import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Header from "./src/Header";
import { useDispatch, useSelector } from "react-redux";
import ContactList from "./src/ContactList";
import ModalAddContact from "./src/ModalAddContact";
import * as Contacts from "expo-contacts";
import { setContacts } from "./actions/contacts";
export default function Application(props) {
  const adding = useSelector((state) => state.showModal);
  const dispatch = useDispatch();
  useEffect(() => {
    const contacts = [];
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        const { data } = await Contacts.getContactsAsync();
        data.forEach((contact) => {
          const newContact = {
            id: contact.id,
            name: contact.name,
            email: contact.emails ? contact.emails[0].email : "No-email",
            phone: contact.phoneNumbers
              ? contact.phoneNumbers[0].number
              : "No phone",
            photo:
              contact.image && contact.image.uri
                ? contact.image.uri
                : "https://image.shutterstock.com/image-vector/avatar-man-icon-profile-placeholder-260nw-1229859850.jpg",
          };
          contacts.push(newContact);
        });
        dispatch(setContacts(contacts));
        console.log(data[data.length - 1]);
      }
    })();
  }, []);
  return (
    <View style={styles.container}>
      <Header />
      <ContactList navigation={props.navigation} />
      {adding ? <ModalAddContact /> : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
});
