const initialState = {
  contacts: [],
  showModal: false,
  search: false,
  searchContacts: [],
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "addContact":
      return {
        ...state,
        contacts: [...state.contacts, action.payload.contact],
      };
    case "showModal":
      return {
        ...state,
        showModal: true,
      };
    case "hideModal":
      return {
        ...state,
        showModal: false,
      };
    case "startSearch":
      return {
        ...state,
        search: true,
      };
    case "endSearch":
      return {
        ...state,
        search: false,
        searchContacts: [],
      };
    case "searchContacts":
      return {
        ...state,
        searchContacts: action.payload.contacts,
      };
    case "watchContact":
      return {
        ...state,
        choosenContact: action.payload.contact,
      };
    case "setContacts":
      return {
        ...state,
        contacts: action.payload.contacts,
        choosenContact: {},
      };
    case "setPhoto":
      const contacts = state.contacts;
      const newContacts = contacts.map((item) => {
        if (+item.id != +action.payload.id) return item;
        else {
          const newUser = {
            ...item,
            photo: action.payload.uri,
          };
          return newUser;
        }
      });
      return {
        ...state,
        contacts: newContacts,
      };
    default:
      return {
        ...state,
      };
  }
};
