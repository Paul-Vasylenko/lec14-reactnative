export const addContact = (contact) => {
  return {
    type: "addContact",
    payload: {
      contact,
    },
  };
};

export const setContacts = (contacts) => {
  return {
    type: "setContacts",
    payload: {
      contacts,
    },
  };
};

export const showModal = () => {
  return {
    type: "showModal",
    payload: {
      adding: true,
    },
  };
};

export const hideModal = () => {
  return {
    type: "hideModal",
    payload: {
      adding: false,
    },
  };
};

export const startSearch = () => {
  return {
    type: "startSearch",
    payload: {
      search: true,
    },
  };
};

export const endSearch = () => {
  return {
    type: "endSearch",
    payload: {
      search: false,
    },
  };
};

export const searchContacts = (contacts) => {
  return {
    type: "searchContacts",
    payload: {
      contacts,
    },
  };
};

export const watchContact = (contact) => {
  return {
    type: "watchContact",
    payload: {
      contact,
    },
  };
};

export const setPhoto = (info) => {
  return {
    type: "setPhoto",
    payload: {
      id: info.id,
      uri: info.uri,
    },
  };
};
