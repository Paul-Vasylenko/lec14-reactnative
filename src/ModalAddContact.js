import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import { useDispatch } from "react-redux";
import { addContact, endSearch, hideModal } from "../actions/contacts";
import * as Contacts from "expo-contacts";
import Contact from "./Contact";

export default function ModalAddContact(props) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const dispatch = useDispatch();
  const createContact = async () => {
    if (name && email && phone) {
      const newContact = {
        id: Date.now().toString(),
        name,
        email,
        phone,
        photo:
          "https://image.shutterstock.com/image-vector/avatar-man-icon-profile-placeholder-260nw-1229859850.jpg",
      };
      dispatch(addContact(newContact));
      setName("");
      setEmail("");
      setPhone("");
      dispatch(hideModal());
      dispatch(endSearch());
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        const androidContact = {
          [Contacts.Fields.FirstName]: name,
          [Contacts.Fields.LastName]: "",
          [Contacts.Fields.Emails]: [
            {
              label: "home",
              email: email,
              type: "1",
              isPrimary: 0,
            },
          ],
          [Contacts.Fields.PhoneNumbers]: [
            {
              label: "mobile",
              number: phone,
              type: "2",
              isPrimary: 0,
            },
          ],
        };
        console.log(androidContact);

        Contacts.addContactAsync(androidContact)
          .then((contactId) => {
            alert("Se creó exitosamente");
          })
          .catch((err) => {
            alert(err);
            console.log("dgfgd");
          });
      }
      // const contactId = await Contacts.addContactAsync(androidContact);
    }
  };
  return (
    <View style={styles.container}>
      <TextInput
        autoCompleteType="name"
        placeholder="Enter contact name"
        style={styles.input}
        value={name}
        onChangeText={(e) => {
          setName(e);
        }}
      />
      <TextInput
        autoCompleteType="email"
        placeholder="Enter contact email"
        style={styles.input}
        value={email}
        onChangeText={(e) => setEmail(e)}
      />
      <TextInput
        autoCompleteType="tel"
        placeholder="Enter contact phone"
        style={styles.input}
        value={phone}
        onChangeText={(e) => setPhone(e)}
      />
      <Button title="Create" style={styles.button} onPress={createContact} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 250,
    backgroundColor: "white",
    borderColor: "black",
    borderWidth: 2,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 150,
    left: 75,
    paddingBottom: 10,
  },
  input: {
    padding: 5,
    margin: 7,
  },
});
