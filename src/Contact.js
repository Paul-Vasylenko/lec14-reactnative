import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { useDispatch } from "react-redux";
import { watchContact } from "../actions/contacts";

export default function Contact({ contact, navigation }) {
  const dispatch = useDispatch();
  const pressHandler = () => {
    dispatch(watchContact(contact));
    navigation.navigate("Contact");
  };
  return (
    <TouchableOpacity onPress={pressHandler} style={styles.container}>
      <Image
        style={styles.img}
        source={{
          uri: contact.photo,
        }}
      />
      <View>
        <Text>{contact.name}</Text>
        <View style={styles.contacts}>
          <Text style={styles.item}>{contact.phone}</Text>
          <Text style={styles.item}>{contact.email}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginLeft: 10,
    padding: 10,
    height: 60,
    borderWidth: 1,
    borderColor: "#000",
    marginTop: 10,
    borderRadius: 5,
  },
  img: {
    width: 50,
    marginRight: 15,
  },
  contacts: {
    flexDirection: "row",
  },
  item: {
    marginRight: 10,
  },
});
