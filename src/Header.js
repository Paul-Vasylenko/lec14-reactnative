import React, { useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import Search from "./Search";
import AddContact from "./AddContact";
import { useDispatch, useSelector } from "react-redux";
import { endSearch } from "../actions/contacts";

export default function Header({ contacts, setContacts }) {
  const addContact = () => {
    const newContact = {
      id: Date.now().toString(),
      name: "",
      phone: "+380",
    };
    setContacts([...contacts, newContact]);
  };
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const searching = useSelector((state) => state.search);
  return (
    <View style={styles.container}>
      <Search search={search} setSearch={setSearch} />
      {searching ? (
        <Button
          title="X"
          onPress={() => {
            dispatch(endSearch());
            setSearch("");
          }}
        />
      ) : null}
      <AddContact onSubmit={addContact} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    padding: 10,
    marginTop: 20,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
});
