import React, { useState } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { setContacts } from "../actions/contacts";
import { Linking } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { setPhoto } from "../actions/contacts";
export default function ContactView(props) {
  const contact = useSelector((state) => state.choosenContact);
  const contacts = useSelector((state) => state.contacts);
  const [tempPhoto, setTempPhoto] = useState(contact.photo);
  const dispatch = useDispatch();
  const onDelete = () => {
    const newContacts = contacts.filter((item) => item.id != contact.id);
    dispatch(setContacts(newContacts));
    props.navigation.goBack();
  };
  const handleChangePhoto = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({});
    if (!result.cancelled) {
      const uri = result.uri;
      dispatch(
        setPhoto({
          id: contact.id,
          uri,
        })
      );
      setTempPhoto(uri);
    }
  };
  const doCall = () => {
    Linking.openURL(`tel:${contact.phone}`);
  };
  return (
    <View style={styles.container}>
      <Image
        style={styles.img}
        source={{
          uri: tempPhoto,
        }}
      />
      <Text style={styles.edit} onPress={handleChangePhoto}>
        Edit
      </Text>
      <View style={styles.info}>
        <Text style={styles.text}>Name: {contact.name}</Text>
        <Text style={styles.text}>Email: {contact.email}</Text>
        <Text style={styles.text}>Phone: {contact.phone}</Text>
      </View>
      <View style={styles.rowbtns}>
        <Button title="Call" onPress={doCall} />
        <Button title="Delete" onPress={onDelete} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 15,
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  edit: {
    fontSize: 18,
    textDecorationLine: "underline",
  },
  info: {
    marginTop: 25,
  },
  text: {
    fontSize: 18,
    padding: 10,
  },
  rowbtns: {
    width: 200,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
