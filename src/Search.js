import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { searchContacts, startSearch } from "../actions/contacts";

export default function Search({ search, setSearch }) {
  const contacts = useSelector((state) => state.contacts);
  const dispatch = useDispatch();
  const doSearch = () => {
    dispatch(startSearch());
    const foundContacts = contacts.filter(
      (contact) =>
        contact.name == search ||
        contact.email == search ||
        contact.phone == search
    );
    dispatch(searchContacts(foundContacts));
  };
  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Search.."
        style={styles.input}
        value={search}
        onChangeText={(e) => setSearch(e)}
      />
      <Button title="Search" style={styles.button} onPress={doSearch} />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 5,
    padding: 5,
    width: 200,
    marginRight: 10,
  },
  container: {
    flexDirection: "row",
  },
  button: {
    width: 30,
    height: 30,
  },
});
