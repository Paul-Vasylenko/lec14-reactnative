import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { useDispatch } from "react-redux";
import { addContact, showModal } from "../actions/contacts";

export default function AddContact(props) {
  const dispatch = useDispatch();
  return (
    <View>
      <Button
        title="+"
        style={styles.button}
        onPress={() => {
          dispatch(showModal());
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "white",
    width: 30,
    height: 30,
  },
});
