import React from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { useSelector } from "react-redux";
import Contact from "./Contact.js";
export default function ContactList({ navigation }) {
  const contacts = useSelector((state) => state.contacts);
  const searchContacts = useSelector((state) => state.searchContacts);
  const searching = useSelector((state) => state.search);

  const renderContact = ({ item }) => {
    return <Contact contact={item} navigation={navigation} />;
  };
  return (
    <View style={styles.container}>
      <FlatList
        data={
          searchContacts.length > 0
            ? searchContacts
            : searching
            ? null
            : contacts
        }
        keyExtractor={(item) => item.id}
        renderItem={renderContact}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
});
