import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import MainView from "../../Application";
import ContactView from "../../src/ContactView";

const ContactNavigator = createStackNavigator({
  Main: {
    screen: MainView,
  },
  Contact: {
    screen: ContactView,
  },
});

export default createAppContainer(ContactNavigator);
